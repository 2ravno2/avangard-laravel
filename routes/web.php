<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::view('order','order/list');
Route::get('order/{order}','\App\Http\Controllers\OrderController@edit');
Route::post('order/{order}','\App\Http\Controllers\OrderController@store');
