<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class OrderProduct extends Model
{
    protected $with = ['product'];

    final public function product():Relation
    {
        return $this->belongsTo('App\Product','product_id');
    }
}
