<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <script src="/js/jquery-3.5.1.min.js.css"></script>

    </head>
    <body>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="POST">
        {{csrf_field()}}
        <label>Email: <input name="client_email" value="{{$order->client_email}}"></label><br>
        <label>Partner:
            <select name="partner_id">
                @foreach(\App\Partner::all() as $partner)
                    <option value="{{$partner->id}}" @if($partner->id==$order->partner_id) selected @endif>{{$partner->name}}</option>
                @endforeach
            </select>
        </label><br>
        Products: {{dump($order->orderProducts->toArray())}}
        Quantity: {{$order->quantity_positions}}<br>
        Amount: {{$order->amount}}<br>
        <label>Status:
            <select name="status">
                @foreach(\App\Order::statuses as $value=>$name)
                    <option value="{{$value}}" @if($value==$order->status) selected @endif>{{$name}}</option>
                @endforeach
            </select>
        </label><br>
        <button type="submit">Submit</button>
    </form>
    </body>
<script>

</script>
</html>
