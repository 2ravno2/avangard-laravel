<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Carbon;

class Order extends Model
{
    // available statuses
    const statuses=[0=>'new',10=>'confirmed',20=>'done'];

    protected $appends = ['quantity_positions'];
    protected $fillable =['client_email','partner_id','status'];

    final public function orderProducts(): Relation
    {
        return $this->hasMany('App\OrderProduct');
    }

    final public function products(): Relation
    {
        return $this->belongsToMany('App\Product','order_products');
    }

    /**
     * Scope a query to only include overdue orders.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    final public function scopeOverdue(Builder $query) : Builder
    {
        return $query->where('delivery_dt', '<', date('Y-m-d'))
            ->where('status',10)
            ->orderByDesc('delivery_dt');
    }

    /**
     * Scope a query to only include current orders.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    final public function scopeCurrent(Builder $query) : Builder
    {
        return $query->whereBetween('delivery_dt', [date('Y-m-d'),Carbon::tomorrow()->toDateString()])
            ->where('status',10)
            ->orderBy('delivery_dt');
    }

    /**
     * Scope a query to only include new orders.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    final public function scopeNew(Builder $query) : Builder
    {
        return $query->where('delivery_dt', '>', date('Y-m-d'))
            ->where('status',0)
            ->orderBy('delivery_dt');
    }

    /**
     * Scope a query to only include done orders.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    final public function scopeDone(Builder $query) : Builder
    {
        return $query->whereDate('delivery_dt', date('Y-m-d'))
            ->where('status',20)
            ->orderByDesc('delivery_dt');
    }

    /**
     * quantity all positions in order
     *
     * @return int
     */
    final public function getQuantityPositionsAttribute():int
    {
        return $this->orderProducts->sum(function ($position){
                return $position->quantity;
            })??0;
    }

    /**
     * total amount of order
     *
     * @return int
     */
    final public function getAmountAttribute():int
    {
        return $this->orderProducts->sum(function ($position){
                return $position->quantity*$position->price;
            })??0;
    }

    /**
     * to set an event by changing status
     * @param int $status
     */
    final public function setStatusAttribute($status)
    {
        $this->status=(int)$status;
        if ($status==20) {
            // TODO: calling event with email send. must do it with jobs
        }
    }
}
